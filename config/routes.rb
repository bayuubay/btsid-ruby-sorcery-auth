# frozen_string_literal: true

Rails.application.routes.draw do
  get 'reset_passwords/new'
  resources :sessions, oonly: %i[new create destroy]
  get '/log_in', to: 'sessions#new', as: :log_in
  delete '/log_out', to: 'sessions#destroy', as: :log_out
  resources :users, only: %i[new create] do
    member do
      get :activate
    end
  end
  resources :reset_passwords, only: %i[new create update edit]
  get '/sign_up', to: 'users#new', as: :sign_up
  get 'users/create'
  get '/secret', to: 'pages#index', as: :secret
  root to: 'pages#index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
