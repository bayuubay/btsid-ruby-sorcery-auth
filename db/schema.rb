# frozen_string_literal: true

# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20_210_604_082_814) do
  # These are extensions that must be enabled in order to support this database
  enable_extension 'plpgsql'

  create_table 'delayed_jobs', force: :cascade do |t|
    t.integer 'priority', default: 0, null: false
    t.integer 'attempts', default: 0, null: false
    t.text 'handler', null: false
    t.text 'last_error'
    t.datetime 'run_at'
    t.datetime 'locked_at'
    t.datetime 'failed_at'
    t.string 'locked_by'
    t.string 'queue'
    t.datetime 'created_at', precision: 6
    t.datetime 'updated_at', precision: 6
    t.index %w[priority run_at], name: 'delayed_jobs_priority'
  end

  create_table 'users', force: :cascade do |t|
    t.string 'email', null: false
    t.string 'crypted_password'
    t.string 'salt'
    t.datetime 'created_at', precision: 6, null: false
    t.datetime 'updated_at', precision: 6, null: false
    t.string 'remember_me_token'
    t.datetime 'remember_me_token_expires_at'
    t.string 'activation_state'
    t.string 'activation_token'
    t.datetime 'activation_token_expires_at'
    t.integer 'failed_logins_count', default: 0
    t.datetime 'lock_expires_at'
    t.string 'unlock_token'
    t.string 'reset_password_token'
    t.datetime 'reset_password_token_expires_at'
    t.datetime 'reset_password_email_sent_at'
    t.integer 'access_count_to_reset_password_page', default: 0
    t.index ['activation_token'], name: 'index_users_on_activation_token'
    t.index ['email'], name: 'index_users_on_email', unique: true
    t.index ['remember_me_token'], name: 'index_users_on_remember_me_token'
    t.index ['reset_password_token'], name: 'index_users_on_reset_password_token'
    t.index ['unlock_token'], name: 'index_users_on_unlock_token'
  end
end
