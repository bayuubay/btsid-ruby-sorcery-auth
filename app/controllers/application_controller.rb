# frozen_string_literal: true

class ApplicationController < ActionController::Base
  before_action :require_login

  private

  def not_authenticated
    flash[:warning] = 'You have to authenticated to access the secret'
    redirect_to log_in_path
  end
end
