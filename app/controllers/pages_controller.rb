# frozen_string_literal: true

class PagesController < ApplicationController
  # before_action :require_login, only: %i[secret]
  def index; end

  def secret; end
end
