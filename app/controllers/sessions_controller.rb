# frozen_string_literal: true

class SessionsController < ApplicationController
  skip_before_action :require_login, except: [:destroy]
  def new; end

  def create
    if login(params[:email], params[:password], params[:remember_me])
      flash[:success] = 'welcome back'
      redirect_back_or_to root_path
    else
      flash.now[:warning] = 'email or password is incorrect!'
      render 'new'
    end
  end

  def destroy
    logout
    flash[:success] = 'good bye!'
    redirect_to log_in_path
  end
end
